[![](banner_karthikeshwar.png)](https://karthikeshwar1.github.io/ka/)

<!-- <body style="background-color:white;"> -->
<h3 align="center">
<!-- <img src="https://readme-typing-svg.herokuapp.com?font=Caveat&color=808080&size=29&center=true&width=500&height=100&lines=Namask%C4%81ra%E1%B8%A5;Hello;Va%E1%B9%87akkam;Namaste;Kon'nichiwa;Hola" alt="greeting in different languages"> -->
<img src="https://readme-typing-svg.herokuapp.com?font=Caveat&color=CF0000&size=29&center=true&width=500&height=100&lines=Namask%C4%81ra%E1%B8%A5;Hello;Va%E1%B9%87akkam;Namaste;Kon'nichiwa;Hola" alt="greeting in different languages">

<!-- [![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Playfair+Display&color=FFFFFF&size=29&center=true&width=500&height=100&lines=Namask%C4%81ra%E1%B8%A5;Hello;Va%E1%B9%87akkam;Namaste;Kon'nichiwa;Hola)](https://git.io/typing-svg) -->

</h3>

<p align="center">I'm a computer science student interested in artificial intelligence, machine learning and human-computer interaction.
<br>
<!-- I'm here to send waves out in the universe.
<br>
Just joking -->
</p>

<br>

| Projects                                                           | Description                                               |
| ------------------------------------------------------------------ | --------------------------------------------------------- |
| [Araam](https://github.com/Karthikeshwar1/Araam)                   | Control your PC with just a controller!                   |
| [TABFY](https://github.com/Karthikeshwar1/TABFY)                   | Take-A-Break-For-Your-Eyes!                               |
| [JumpG](https://github.com/Karthikeshwar1/JumpG)                   | A Windows console game inspired from Google's T-rex game! |
| [Etcch-A-Sketch](https://github.com/Karthikeshwar1/Etcch-A-Sketch) | Drawing app inspired from the popular toy Etch A Sketch   |

<br>

| Mini-projects                                                                                | Description                                                  |
| -------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| [screenRec](https://github.com/Karthikeshwar1/screenRec)                                     | A screen recorder to take screenshots and record video       |
| [controller-vibration-tester](https://github.com/Karthikeshwar1/controller-vibration-tester) | A python script to test vibration motors of game controllers |
| [Misc](https://github.com/Karthikeshwar1/Misc)                                               | miscellaneous                                                |
| [Wiki-Wordcloud-Maker](https://github.com/Karthikeshwar1/Wiki-Wordcloud-Maker)               | Creates a wordcloud image based on a given Wikipedia article |

<br>

<h3 align="center">

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=Karthikeshwar1&hide=java&langs_count=4&custom_title=most%20used%20langugages&theme=dark)](https://github.com/anuraghazra/github-readme-stats)
[![GitHub stats](https://github-readme-stats.vercel.app/api?username=karthikeshwar1&custom_title=github%20stats&line_height=33&theme=dark)](https://github.com/anuraghazra/github-readme-stats)

</h3>

<br>

<h3 align="center">

[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Playfair+Display&color=808080&size=29&center=true&width=500&height=100&lines=Get+lost;Tolagi+h%C5%8Dgu;bhaad+mein+jao;Tolaintu+p%C5%8D;Usero;Skedaddle)](https://git.io/typing-svg)

</h3>

<br>

[![Github website link](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/karthikeshwar1/ka)
[![Linkedin](https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)](https://linkedin.com/in/karthikeshwar/)
[![Spotify](https://img.shields.io/badge/Spotify-1ED760?style=for-the-badge&logo=spotify&logoColor=white)](https://spotify.com)
